﻿using PhoneBook.Behavior.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoneBook.Models
{
    public class Phone : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Емейл")]
        public string Location { get; set; }

        [Display(Name = "Телефон")]
        public string PhoneNumber { get; set; }
    }
}