﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PhoneBook.Models
{
    public class PhoneBookContext : DbContext
    {

        public PhoneBookContext() : base("DefaultConnection")
        {
           
        }
        public DbSet<Phone> Phones{ get; set; }
    }
}