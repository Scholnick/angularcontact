﻿using PhoneBook.Behavior.Interfaces;
using PhoneBook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PhoneBook.Services
{
    public class PhoneService
    {
        PhoneBookContext db;
        public PhoneService()
        {
            db = new PhoneBookContext();
        }
        public IEnumerable<Phone> Get()
        {
            return db.Phones.ToList();
        }
        public Phone Get(int id)
        {
            var phone = db.Phones.Where(i => i.Id == id).FirstOrDefault();
            if (phone != null)
            {
                return phone;
            }
            return null;
        }
        public Phone Create(Phone phone)
        {
            if (phone != null)
            {
                db.Phones.Add(phone);
            }
            return phone;
        }
        public Phone Update(Phone item)
        {
            Phone phone = db.Phones.Find(item.Id);
            if (phone != null)
            {
                phone.Name = item.Name;
                phone.Location = item.Location;
                phone.PhoneNumber = item.PhoneNumber;
            }
            return item;
        }
        public void Delete(Phone phone)
        {
            if (phone != null)
            {
                db.Entry(phone).State = EntityState.Deleted;
            }
        }
        public void Save()
        {
            db.SaveChanges();
        }
    }
}