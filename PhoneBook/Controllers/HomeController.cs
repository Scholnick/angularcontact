﻿using PhoneBook.Models;
using PhoneBook.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace PhoneBook.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            PhoneService productServ = new PhoneService();
            List<Phone> ph = new List<Phone>() {
                new Phone() { Name = "Phone 1", Location = "email", PhoneNumber = "911" },
                new Phone() { Name = "Phone 1", Location = "email", PhoneNumber = "911" },
                new Phone() { Name = "Phone 1", Location = "email", PhoneNumber = "911" },
                new Phone() { Name = "Phone 1", Location = "email", PhoneNumber = "911" }};

            productServ.Create(
                new Phone() { Name = "Phone 1", Location = "email", PhoneNumber = "911" }
                );

            productServ.Save();
            return View();
        }
    }
}
