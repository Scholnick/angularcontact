﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using PhoneBook.Models;
using System.Web.Http;
using PhoneBook.Services;
using System.Web.Http.Cors;

namespace PhoneBook.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [RoutePrefix("api/Phones")]
    public class PhonesController : ApiController
    {
        private PhoneService _phoneService;


        public PhonesController()
        {
            _phoneService = new PhoneService();
        }
        [HttpGet]
        public IEnumerable<Phone> Get()
        {
            return _phoneService.Get();
        }

        [HttpGet]
        public Phone Get(int id)
        {
            Phone phone = _phoneService.Get(id);
            if (phone == null)
            {

            }
            return phone;
        }
        [HttpPost]
        public Phone CreatePhone(Phone phone)
        {
            if (ModelState.IsValid)
            {
                Phone _phone = _phoneService.Create(phone);
                _phoneService.Save();
            }
            return phone;
        }
        [HttpPut]
        [Route("EditPhone")]
        public Phone EditPhone(Phone phone)
        {
            if (ModelState.IsValid)
            {
                _phoneService.Update(phone);
                _phoneService.Save();
                return phone;
            }
            return phone;
        }

        [HttpDelete]
        [Route("DeletePhone")]
        public string DeletePhone(int Id)
        {
            var phone = _phoneService.Get(Id);
            if (phone != null)
            {
                _phoneService.Delete(phone);
                _phoneService.Save();
                return "not deleted";
            }
            return "deleted";
        }
    }
}
