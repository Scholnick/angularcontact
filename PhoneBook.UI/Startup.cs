﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PhoneBook.UI.Startup))]
namespace PhoneBook.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
