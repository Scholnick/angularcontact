﻿var app = angular.module("PhoneApp", []);
app.controller("PhoneController", function ($scope, $http) {
    $scope.domenUrl = "http://localhost:12965/";
    $scope.sortType = 'Name'; // значение сортировки по умолчанию
    $scope.sortReverse = false;  // обратная сортривка
    $scope.editFrom = true;
    $scope.createForm = true;
    $scope.showCreateForm = function () {
        $scope.createForm = false;
        $scope.editFrom = true;
    }
    $scope.headers = ["Name", "Location", "PhoneNumber"];
    $scope.refresh = function myfunction() {
        $http({
            method: 'GET',
            url: $scope.domenUrl + 'api/Phones/',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            withCredentials: true
        }).then(function successCallback(response) {
            $scope.PhoneList = response.data;
        }, function errorCallback(response) {
        })
    }
    $http({
        method: 'GET',
        url: $scope.domenUrl + 'api/Phones/',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        withCredentials: true
    }).then(function successCallback(response) {
        $scope.PhoneList = response.data;
    }, function errorCallback(response) {

    })
    $scope.clearContact = function () {
        //$scope.UpdateContact.Id = '';
        $scope.UpdateContact.Name = '';
        $scope.UpdateContact.Location = '';
        $scope.UpdateContact.PhoneNumber = '';
    }
    $scope.clearNewPhone = function clearNewPhone() {
        //$scope.Phone.Id = '';
        $scope.Phone.Name = '';
        $scope.Phone.Location = '';
        $scope.Phone.PhoneNumber = '';
    }
    $scope.cancel = function cancelUpdate() {
        $scope.UpdateContact.Id = 0;
        $scope.UpdateContact.Name = '';
        $scope.UpdateContact.Location = '';
        $scope.UpdateContact.PhoneNumber = '';
        $scope.editFrom = true;
    }
    $scope.Phone = {
        Id: 0,
        Name: '',
        Location: '',
        PhoneNumber: ''
    };
    $scope.UpdateContact = {
        Id: 0,
        Name: '',
        Location: '',
        PhoneNumber: ''
    };

    $scope.PhoneViewModel = {
        Name: '',
        Location: '',
        PhoneNumber: ''
    };
    $scope.saveContact = function () {
        if ($scope.Phone.Name != "" && $scope.Phone.Location != "" && $scope.Phone.PhoneNumber != "") {
            console.log('have some data');
            console.log($scope.Phone);
            $scope.Phone.Id = 0;
        }
        $http({
            method: 'POST',
            url: $scope.domenUrl + 'api/Phones/Create/',
            data: $scope.Phone
        }).then(function successCallback(response) {
            $scope.PhoneList.push(response.data);
            $scope.clearNewPhone();
        }, function errorCallback(response) {
            console.log("Error : " + response.data.ExceptionMessage);
        });
    }
    $scope.saveChangesContact = function () {
        if ($scope.UpdateContact.Name != "" || $scope.UpdateContact.Location != "" || $scope.UpdateContact.PhoneNumber != "") {
            
            $http.put($scope.domenUrl + 'api/Phones/EditPhone/', $scope.UpdateContact, { headers: { 'Content-Type': 'application/json' } }).then(function successCallback(response) {
                $scope.PhoneList.push(response.data);
                console.log(response.data);
            }, function errorCallback(response) {
                console.log("Error : " + response.data.ExceptionMessage);
            });
        }
    }
    function getContactById(Id) {
        $http({
            method: 'GET',
            url: $scope.domenUrl + 'api/Phones/' + Id
        }).then(function successCallback(response) {
            $scope.UpdateContact = response.data;
        }, function errorCallback(response) {
            console.log("Error : " + response.data.ExceptionMessage);
        });
    }
    $scope.edit = function (index) {
        $scope.editFrom = false;
        $scope.createForm = true;
        getContactById(index);
    }
    $scope.delete = function (index) {
        $scope.UrlQuery = $scope.domenUrl + 'api/Phones/DeletePhone?Id=' + index;
        $http({
            method: 'DELETE',
            url: $scope.UrlQuery,
            data: { Id: index },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8' },
            withCredentials: true
        }).then(function successCallback(response) {
            $scope.refresh();
            //var indexPhone = $scope.PhoneList.indexOf(index);
            //$scope.PhoneList.splice(indexPhone, 1);
        }, function errorCallback(response) {
            console.log("Error ");
        });
    }

});